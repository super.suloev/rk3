#include "../includes/generate_matrix.h"

void generate_incidence(Input_t *input_p) {
    short matrix[input_p->edges_num][input_p->vertexes_num];
    for (unsigned long long i = 0; i < input_p->edges_num; ++i)
        for (unsigned long long j = 0; j < input_p->vertexes_num; ++j)
            matrix[i][j] = 0;

    Edge_t *this_edge_p;
    this_edge_p = input_p->head_p;

    for (unsigned long long i = 0; i < input_p->edges_num; ++i) {
        matrix[i][this_edge_p->from - 1] = 1;
        matrix[i][this_edge_p->to - 1] = 1;
        this_edge_p = this_edge_p->next_edge_p;
    }

    FILE *fp;
    fp = fopen("../incidence_matrix.txt", "w");

    for (unsigned long long j = 0; j < input_p->vertexes_num; ++j) {
        for (unsigned long long i = 0; i < input_p->edges_num; ++i)
            fprintf(fp, "%d", matrix[i][j]);
        fprintf(fp, "\n");
    }

    fclose(fp);
}




void generate_adjacency(Input_t *input_p) {
    short matrix[input_p->vertexes_num][input_p->vertexes_num];
    for (unsigned long long i = 0; i < input_p->vertexes_num; ++i)
        for (unsigned long long j = 0; j < input_p->vertexes_num; ++j)
            matrix[i][j] = 0;

    Edge_t *this_edge_p;
    this_edge_p = input_p->head_p;
    for (unsigned long long i = 0; i < input_p->edges_num; ++i) {
        matrix[this_edge_p->from - 1][this_edge_p->to - 1] = 1;
        matrix[this_edge_p->to - 1][this_edge_p->from - 1] = 1;
        this_edge_p = this_edge_p->next_edge_p;
    }

    FILE *fp;
    fp = fopen("../adjacency_matrix.txt", "w");

    for (unsigned long long j = 0; j < input_p->vertexes_num; ++j) {
        for (unsigned long long i = 0; i < input_p->vertexes_num; ++i)
            fprintf(fp, "%d", matrix[i][j]);
        fprintf(fp, "\n");
    }

    fclose(fp);
}