#include "../includes/get_data.h"

void parse_arguments(int argc, char *argv[], Input_t *input_p) {
    input_p->head_p = NULL;
    input_p->need_adjacency = False;
    input_p->need_incidence = False;
    input_p->edges_num = 0;
    input_p->vertexes_num = 0;

    char arg;
    while ((arg = (char) getopt(argc, argv, "n:e:ai")) != -1) {
        switch (arg) {
            case 'n':
                input_p->vertexes_num = strtoull(optarg, NULL, 10);
                break;

            case 'e':
                input_p->edges_num = strtoull(optarg, NULL, 10);
                break;

            case 'a':
                input_p->need_adjacency = True;
                break;

            case 'i':
                input_p->need_incidence = True;
                break;

            default:
                printf("UNKNOWN ARGUMENT %c", arg);
                exit(1);
        }
    }

    if (input_p->vertexes_num == 0) {
        printf("THE NUMBER OF EDGES WAS NOT SET");
        exit(1);
    }
    if (input_p->edges_num == 0) {
        printf("THE NUMBER OF VERTEXES WAS NOT SET");
        exit(1);
    }
    if (input_p->need_incidence + input_p->need_adjacency < 1) {
        printf("THE MATRIX TYPE WAS NOT SPECIFIED");
        exit(1);
    }
}