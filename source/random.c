#include "../includes/random.h"

unsigned long long generate_number(unsigned long long max_num) {
    unsigned long long random_number = ((unsigned long long)rand() << 32) | rand();
    random_number = random_number % max_num + 1;
    return random_number;
}

void generate_random_matrix(Input_t *input_p) {
    srand(time(NULL));
    for (unsigned long long i = 1; i <= input_p->edges_num; ++i) {
        Edge_t *new_edge_p;
        new_edge_p = (Edge_t*) malloc(sizeof(Edge_t));
        new_edge_p->from = generate_number(input_p->vertexes_num);
        new_edge_p->to = generate_number(input_p->vertexes_num);
        new_edge_p->next_edge_p = NULL;
        if (input_p->head_p == NULL)
            input_p->head_p = new_edge_p;
        else
            input_p->tail_p->next_edge_p = new_edge_p;
        input_p->tail_p = new_edge_p;
    }
}
