#include "../includes/visualization.h"

void make_dot_file(Input_t *input_p) {
    FILE *fp;
    fp = fopen("../graph.dot", "w");
    fprintf(fp, "graph G {\n");
    for (unsigned long long i = 1; i <= input_p->vertexes_num; ++i)
        fprintf(fp, "\t%llu;\n", i);

    Edge_t *this_edge_p;
    this_edge_p = input_p->head_p;

    for (unsigned long long i = 0; i < input_p->edges_num; ++i) {
        fprintf(fp, "\t%llu -- %llu;\n", this_edge_p->from, this_edge_p->to);
        this_edge_p = this_edge_p->next_edge_p;
    }

    fprintf(fp, "}");

    fclose(fp);
}


void generate_image() {
    system("dot -Tpng ../graph.dot -o ../output.png");
}