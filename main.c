#include "includes/main.h"

int main(int argc, char *argv[]) {
    Input_t *input_p;
    input_p = (Input_t*) malloc(sizeof(Input_t));

    parse_arguments(argc, argv, input_p);
    generate_random_matrix(input_p);

    Edge_t *this_edge_p;
    this_edge_p = input_p->head_p;
    if (input_p->need_incidence)
        generate_incidence(input_p);

    if (input_p->need_adjacency)
        generate_adjacency(input_p);

    make_dot_file(input_p);
    generate_image();

    return 0;
}
