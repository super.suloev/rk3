#ifndef BC3_GENERATE_MATRIX_H
#define BC3_GENERATE_MATRIX_H

#include "data.h"
#include <stdio.h>

void generate_incidence(Input_t *input_p);
void generate_adjacency(Input_t *input_p);

#endif
