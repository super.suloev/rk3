#ifndef BC3_RANDOM_H
#define BC3_RANDOM_H

#include "data.h"
#include <time.h>
#include <stdlib.h>
#include <stdio.h>

unsigned long long generate_number(unsigned long long max_num);
void generate_random_matrix(Input_t *input_p);

#endif
