#ifndef BC3_GET_DATA_H
#define BC3_GET_DATA_H

#include "data.h"
#include <stdlib.h>
#include <getopt.h>
#include <stdio.h>

void parse_arguments(int argc, char *argv[], Input_t *input_p);

#endif