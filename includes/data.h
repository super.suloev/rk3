#ifndef BC3_DATA_H
#define BC3_DATA_H

#define True 1
#define False 0
#define and &&
#define or ||

typedef struct Edge_t {
    unsigned long long from, to;
    struct Edge_t *next_edge_p;
} Edge_t;

typedef struct {
    unsigned long long edges_num, vertexes_num;
    short need_incidence, need_adjacency;
    struct Edge_t *head_p, *tail_p;
} Input_t;

#endif
