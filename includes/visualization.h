#ifndef BC3_VISUALIZATION_H
#define BC3_VISUALIZATION_H

#include "data.h"
#include <stdio.h>
#include <stdlib.h>

void make_dot_file(Input_t *input_p);
void generate_image();

#endif
